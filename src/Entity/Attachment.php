<?php

namespace dlouhy\EmailBundle\Entity;

class Attachment
{
    /**
     * @var \Swift_Mime_Attachment
     */
    private $file;

    /**
     * @var bool
     */
    private $inline;

    /**
     * @var string
     */
    private $inlineWildcard;

    /**
     * @return \Swift_Mime_Attachment
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param \Swift_Mime_Attachment $file
     * @return Attachment
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * @return bool
     */
    public function isInline()
    {
        return $this->inline;
    }

    /**
     * @param bool $inline
     * @return Attachment
     */
    public function setInline($inline)
    {
        $this->inline = $inline;

        return $this;
    }

    /**
     * @return string
     */
    public function getInlineWildcard()
    {
        return $this->inlineWildcard;
    }

    /**
     * @param string $inlineWildcard
     * @return Attachment
     */
    public function setInlineWildcard($inlineWildcard)
    {
        $this->inlineWildcard = $inlineWildcard;

        return $this;
    }
}
