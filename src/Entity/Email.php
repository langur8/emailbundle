<?php

namespace dlouhy\EmailBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="eml__emails")
 * @ORM\HasLifecycleCallbacks()
 */
class Email
{

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $subject;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $sender;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $recipient;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $replyTo;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $message;

    /**
     * @var EmailPrototype|null
     * @ORM\ManyToOne(targetEntity="EmailPrototype")
     * @ORM\JoinColumn(name="eml__email_prototype_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $emailPrototype;


    /**
     * @var string
     */
    protected $logo;

    /**
     * @var string
     */
    protected $label;

    /**
     * @var string
     */
    protected $header;

    /**
     * @var string
     */
    protected $lead;

    /**
     * @var string
     */
    protected $contentHtml;

    /**
     * @var string
     */
    protected $contentText;

    /**
     * @var string
     */
    protected $footer;

    /**
     * @var string
     */
    protected $footerMenu;

    /**
     * @var array
     */
    protected $wildcards;

    /**
     * @var Attachment[]
     */
    protected $attachments = [];

    /**
     * @param EmailPrototype|null $emailPrototype
     */
    public function __construct(EmailPrototype $emailPrototype = null)
    {
        if ($emailPrototype !== null) {
            $this->emailPrototype = $emailPrototype;
            $this->subject = $emailPrototype->getSubject();
            $this->sender = $emailPrototype->getSender();
            $this->recipient = $emailPrototype->getRecipient();
            $this->replyTo = $emailPrototype->getReplyTo();
            $this->contentText = $emailPrototype->getBodyText();
            $this->contentHtml = $emailPrototype->getBodyHtml();
        }
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->created = new \DateTime;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    //GENERATED

    /**
     * @param int $id
     * @return Email
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     * @return Email
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     * @return Email
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return string
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param string $sender
     * @return Email
     */
    public function setSender($sender)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * @return string
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @param string $recipient
     * @return Email
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * @return string
     */
    public function getReplyTo()
    {
        return $this->replyTo;
    }

    /**
     * @param string $replyTo
     * @return Email
     */
    public function setReplyTo($replyTo)
    {
        $this->replyTo = $replyTo;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return Email
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return EmailPrototype|null
     */
    public function getEmailPrototype()
    {
        return $this->emailPrototype;
    }

    /**
     * @param EmailPrototype|null $emailPrototype
     * @return Email
     */
    public function setEmailPrototype($emailPrototype)
    {
        $this->emailPrototype = $emailPrototype;

        return $this;
    }

    /**
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     * @return Email
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return Email
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return string
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * @param string $header
     * @return Email
     */
    public function setHeader($header)
    {
        $this->header = $header;

        return $this;
    }

    /**
     * @return string
     */
    public function getLead()
    {
        return $this->lead;
    }

    /**
     * @param string $lead
     * @return Email
     */
    public function setLead($lead)
    {
        $this->lead = $lead;

        return $this;
    }

    /**
     * @return string
     */
    public function getContentHtml()
    {
        return $this->contentHtml;
    }

    /**
     * @param string $contentHtml
     * @return Email
     */
    public function setContentHtml($contentHtml)
    {
        $this->contentHtml = $contentHtml;

        return $this;
    }

    /**
     * @return string
     */
    public function getContentText()
    {
        return $this->contentText;
    }

    /**
     * @param string $contentText
     * @return Email
     */
    public function setContentText($contentText)
    {
        $this->contentText = $contentText;

        return $this;
    }

    /**
     * @return string
     */
    public function getFooter()
    {
        return $this->footer;
    }

    /**
     * @param string $footer
     * @return Email
     */
    public function setFooter($footer)
    {
        $this->footer = $footer;

        return $this;
    }

    /**
     * @return string
     */
    public function getFooterMenu()
    {
        return $this->footerMenu;
    }

    /**
     * @param string $footerMenu
     * @return Email
     */
    public function setFooterMenu($footerMenu)
    {
        $this->footerMenu = $footerMenu;

        return $this;
    }

    /**
     * @return array
     */
    public function getWildcards()
    {
        return $this->wildcards;
    }

    /**
     * @param array $wildcards
     * @return Email
     */
    public function setWildcards($wildcards)
    {
        $this->wildcards = $wildcards;

        return $this;
    }

    /**
     * @return Attachment[]
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * @param Attachment[] $attachments
     * @return Email
     */
    public function setAttachments($attachments)
    {
        $this->attachments = $attachments;

        return $this;
    }
}
