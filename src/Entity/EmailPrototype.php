<?php

namespace dlouhy\EmailBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="eml__email_prototypes")
 * @ORM\HasLifecycleCallbacks()
 */
class EmailPrototype
{

	/**
	 * @var int
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;
	
	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $modified;	

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 */
	protected $subject;

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 */
	protected $sender;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $recipient;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $replyTo;

	/**
	 * @var boolean
	 * @ORM\Column(type="boolean")
	 */
	protected $sendHtml = true;

	/**
	 * @var boolean
	 * @ORM\Column(type="boolean")
	 */
	protected $sendText = false;

	/**
	 * @var string
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $bodyHtml;

	/**
	 * @var string
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $bodyText;
	
	/**
	 * @ORM\PreUpdate
	 */
	public function preUpdate()
	{
		$this->modified = new \DateTime;
	}

	/**
	 * @ORM\PrePersist
	 */
	public function prePersist()
	{
		$this->created = new \DateTime;
	}

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return EmailPrototype
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     * @return EmailPrototype
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     * @return EmailPrototype
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     * @return EmailPrototype
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return string
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param string $sender
     * @return EmailPrototype
     */
    public function setSender($sender)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * @return string
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @param string $recipient
     * @return EmailPrototype
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * @return string
     */
    public function getReplyTo()
    {
        return $this->replyTo;
    }

    /**
     * @param string $replyTo
     * @return EmailPrototype
     */
    public function setReplyTo($replyTo)
    {
        $this->replyTo = $replyTo;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSendHtml()
    {
        return $this->sendHtml;
    }

    /**
     * @param bool $sendHtml
     * @return EmailPrototype
     */
    public function setSendHtml($sendHtml)
    {
        $this->sendHtml = $sendHtml;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSendText()
    {
        return $this->sendText;
    }

    /**
     * @param bool $sendText
     * @return EmailPrototype
     */
    public function setSendText($sendText)
    {
        $this->sendText = $sendText;

        return $this;
    }

    /**
     * @return string
     */
    public function getBodyHtml()
    {
        return $this->bodyHtml;
    }

    /**
     * @param string $bodyHtml
     * @return EmailPrototype
     */
    public function setBodyHtml($bodyHtml)
    {
        $this->bodyHtml = $bodyHtml;

        return $this;
    }

    /**
     * @return string
     */
    public function getBodyText()
    {
        return $this->bodyText;
    }

    /**
     * @param string $bodyText
     * @return EmailPrototype
     */
    public function setBodyText($bodyText)
    {
        $this->bodyText = $bodyText;

        return $this;
    }
}
