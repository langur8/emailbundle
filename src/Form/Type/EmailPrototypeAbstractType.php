<?php

namespace dlouhy\EmailBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class EmailPrototypeAbstractType extends AbstractType
{

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('subject', 'text', array(
					'label' => 'Předmět',
					'required' => true
				))
				->add('sender', 'text', array(
					'label' => 'Odesílatel',
					'required' => true
				))
				->add('replyTo', 'text', array(
					'label' => 'Odpověď na',
					'required' => false
				))			
				->add('sendHtml', 'checkbox', array(
					'label' => 'Odesílat jako HTML',
					'required' => false
				))
				->add('bodyHtml', 'textarea', array(
					'label' => 'HTML',
					'required' => false,
					'attr' => array(
						'rows' => 10
					)
				))
				->add('sendText', 'checkbox', array(
					'label' => 'Odesílat jako text',
					'required' => false
				))
				->add('bodyText', 'textarea', array(
					'label' => 'Text',
					'required' => false,
					'attr' => array(
						'rows' => 10
					)					
				))
				->add('save', 'submit', array('label' => 'Uložit'));
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'dlouhy\EmailBundle\Entity\EmailPrototype'
		));
	}

	public function getName()
	{
		return 'email_bundle';
	}
}
	