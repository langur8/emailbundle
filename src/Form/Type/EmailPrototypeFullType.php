<?php

namespace dlouhy\EmailBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmailPrototypeFullType extends EmailPrototypeAbstractType
{

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		parent::buildForm($builder, $options);
		$builder
				->add('recipient', 'text', array(
					'label' => 'Příjemce',
					'required' => true
				));
	}

}
	