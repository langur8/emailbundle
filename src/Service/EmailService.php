<?php

namespace dlouhy\EmailBundle\Service;

use dlouhy\EmailBundle\Entity\Email;
use dlouhy\EmailBundle\Entity\EmailPrototype;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Templating\EngineInterface;

class EmailService
{

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @param \Swift_Mailer   $mailer
     * @param EngineInterface $templating
     * @param Logger          $logger
     */
    public function __construct(\Swift_Mailer $mailer, EngineInterface $templating, Logger $logger)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->logger = $logger;
    }

    /**
     * @param Email $email
     */
    public function send(Email $email)
    {
        $sendPlain = false;
        $sendHtml = true;

        if ($email->getEmailPrototype() instanceof EmailPrototype) {
            $sendPlain = $email->getEmailPrototype()->isSendText();
            $sendHtml = $email->getEmailPrototype()->isSendHtml();
        }

        if ($email->getWildcards() !== null) {
            foreach ($email->getWildcards() as $wildcard => $content) {
                if (isset($content['html'])) {
                    $email->setContentHtml(
                        str_replace($wildcard, $content['html'], $email->getContentHtml())
                    );
                }
                if (isset($content['text'])) {
                    $email->setContentText(
                        str_replace($wildcard, $content['text'], $email->getContentText())
                    );
                }
            }
        }

        $message = \Swift_Message::newInstance();
        $message->setSubject($email->getSubject());
        $message->setFrom($email->getSender());
        $message->setTo($email->getRecipient());
        if ($email->getReplyTo() !== null) {
            $message->setReplyTo($email->getReplyTo());
        }

        foreach ($email->getAttachments() as $attachment) {
            if ($attachment->isInline()) {
                $sendPlain = false;
                $attachment->getFile()->setDisposition('inline');
                $email->setContentHtml(
                    str_replace($attachment->getInlineWildcard(), $message->embed($attachment->getFile()), $email->getContentHtml())
                );
            } else {
                $message->attach($attachment->getFile());
            }
        }

        if ($sendHtml === true) {
            $contentHtml = $this->templating->render('dlouhyEmailBundle:Email:html_email.html.twig', ['email' => $email]);
        }
        if ($sendPlain === true) {
            $contentPlain = $this->templating->render('dlouhyEmailBundle:Email:plain_email.html.twig', ['email' => $email]);
        }
        if ($sendHtml === true && $sendPlain === true) {
            $message->setBody($contentPlain, 'text/plain');
            $message->addPart($contentHtml, 'text/html');
        } else if ($sendPlain === true) {
            $message->setBody($contentPlain, 'text/plain');
        } else {
            $message->setBody($contentHtml, 'text/html');
        }

        $email->setMessage($message->__toString());
        $this->mailer->send($message);
        $this->logger->info(json_encode(['to' => $message->getTo(), 'subject' => $message->getSubject(), 'from' => $message->getFrom()]));
    }
}
